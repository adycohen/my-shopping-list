var express = require("express");
var app = express();
app.listen(9090);
app.use(express.static(__dirname));

var mongoose = require("mongoose");


const ProductSchema = new mongoose.Schema({
    _id:Number,
    name:String,
    quantity:Number,
    category:String
}, { versionKey:false });


const Products = mongoose.model('product', ProductSchema);

//Enables to recieve the data from the client
var bodyparser = require("body-parser");
app.use(bodyparser.urlencoded({extended:true}));
app.use(bodyparser.json());

app.get("/", function(req, res){
    res.sendFile(__dirname + "/index.html");
});

//CRUD - Products
app.get("/getproducts", function(req, res){
    mongoose.connect("mongodb://localhost/shoppinglist",
    { useNewUrlParser: true });
    Products.find(function(err, data){
        if(err)
        {
            console.log(err);
            res.send("Failed");
        }
        else
        {
            console.log(data);
            res.send(data);
        }
        mongoose.connection.close();
    });
});

app.post("/insertproduct", function(req, res){
    mongoose.connect("mongodb://localhost/shoppinglist",
    { useNewUrlParser: true });
    console.log(req.body);
    var newProduct = new Products({
        _id:req.body._id, 
        name:req.body.name,
        quantity:req.body.quantity,
        category:req.body.category
    });
    newProduct.save(function(err){
        if(err)
        {
            console.log(err);
            res.send("Failed");
        }
        else
        {
            res.send("Successfully Inserted Product");
        }
        mongoose.connection.close();
    });
});

app.put("/updateproduct", function(req, res){
    setTimeout(() => {
        mongoose.connect("mongodb://localhost/shoppinglist",
        {useNewUrlParser: true});
        console.log(req.body);

        Products.findOne({_id:req.body._id}, function(err, data)
        {
            if(err)
            {
                console.log(err);
                res.send("Failed");
            }
            else
            {
                console.log(data);
                data.name = req.body.name;
                data.quantity = req.body.quantity;
                data.category = req.body.category;
                data.save(function(err){
                    if(err)
                    {
                        console.log(err);
                        res.send("Failed");
                    }
                    else
                    {
                        res.send("Successfully Updated Product");
                    }
                    mongoose.connection.close();
                });
            }
        });
    }, 3000);
});

app.delete("/deleteproduct", function(req, res){
    mongoose.connect("mongodb://localhost/shoppinglist",
    { useNewUrlParser: true });
    console.log(req.query);

    Products.deleteOne({_id: req.query._id},function(err,data){ 
        if(err)
        {
            console.log(err);
            res.send("Failed");
        }
        else
        {
            console.log(data);
            res.send("Successfully Deleted Product");
        }
        mongoose.connection.close();
    });
});
